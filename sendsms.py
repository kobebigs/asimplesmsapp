#!/usr/bin/env python
#------------------------------------------------------
# Description: sendsms.py - Used to send text messages.
# Author: SC/ITC/09/0004
#	 Kobe Subramaniam
# Date:  Tuesday, April 2, 2013
#
#------------------------------------------------------

import serial
import time
 
class TextMessage:
    def __init__(self, recipient="", message="Hi! Default message sent via Samba75."):
        self.recipient = recipient
        self.content = message
	print '\nsuccessfully started\n'
        
    def setRecipient(self, number):
	self.recipient = number
 
    def setContent(self, message):
        self.content = message
 
    def connectModem(self):
        self.ser = serial.Serial('/dev/ttyACM0', 460800, timeout=5)
        time.sleep(1)

    def sendMessage(self):
	self.ser.write('ATZ\r')
	time.sleep(1)
	self.ser.write('AT+CMGF=1\r')
	time.sleep(1)
	self.ser.write('''AT+CMGS="''' + self.recipient + '''"\r''')
	time.sleep(1)
	self.ser.write(self.content + "\r")
	time.sleep(1)
	self.ser.write(chr(26))
	time.sleep(5)
	print 'Message sent\n'

    def disconnectModem(self):
	self.ser.close()	

#if __name__ == "__main__":
# 	sms = TextMessage("0248317498", "Hi! Good morning. --kobebigs-pc")
# 	sms.connectModem()
#	sms.sendMessage()
#	sms.disconnectModem()


