#------------------------
# Description: app.py - confimessages.
# Author: SC/ITC/09/0004
#	 	  Kobe Subramaniam
# Date:  Monday, April 22, 2013
#------------------------

#imports
import os
import serial
import StringIO
import datetime
import random

from sendsms import TextMessage
#from getdata import getDataFromArduino

from flask import Flask, render_template, request, url_for, redirect, make_response

from pylab import * 
from matplotlib.mlab import csv2rec
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from pygooglechart import *


#configuration

#create app
app = Flask(__name__)

# read data from arduino

#index
@app.route('/')
def home():
	return render_template('index.html')

@app.route('/', methods=['POST'])	
def post():
	error = None

	#send message if value is crucial
	recipients_list = request.form['recipient']
	message = request.form['message']

	if recipients_list != '' and message != '':

		#check if multiple recipients
		recipients = recipients_list.split(',')

		#call TextMessage class and connect modem
		sambaConnect = TextMessage()
		sambaConnect.connectModem()
		
		if len(recipients) >= 1:
			for contact in recipients:
				#contact = contact.strip()
				#set recipient and send message 
				sambaConnect.setRecipient(str(contact))
				sambaConnect.setContent(str(message))
				sambaConnect.sendMessage()				 
		
		#disconnect modem
		sambaConnect.disconnectModem()
	else:
		error = 'Leave no empty field'	

	return redirect('/')

#dashboard
@app.route('/dashboard')
def dashboard():
	
	#read temperature data file delimiter tab - \t
	data = csv2rec('data.txt', delimiter='\t', converterd={'time': str})
	
	return render_template('dashboard.html', data=data)

#docs - wiki
@app.route('/wiki')
def wiki():
	return render_template('wiki.html')

#about
@app.route('/about')
def about():
	return render_template('about.html')

@app.errorhandler(404)
def page_not_found(e):
	return render_template('404.html'), 404



if __name__ == '__main__':
	#Bind to PORT if defined, otherwise default to 5000
	port = int(os.environ.get('PORT', 5000))
	app.run(debug=True, host='0.0.0.0', port=port)
	#app.run(debug=True)
