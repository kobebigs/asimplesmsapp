/**
 * send random generated values from 0 to 60 to PC
 * using serial communication
 *
 * @kobebigs Mon May 6, 2013 21:41:30
 */ 
 
#include <Time.h>

void setup()
{
  // start serial port at 9600 bps:
  Serial.begin(9600);

  establishContact();  // send a byte to establish contact until receiver responds 
}

void loop()
{
 
}

void establishContact() {
  while (Serial.available() <= 0) {
    //Serial.print(',');   // separate values by tab
    //Serial.print(random(0, 60));   // send a random number btn 0 & 60
    //Serial.print('\t');   // separate values by tab
    Serial.println(random(0, 40));   // send a random number btn 0 & 60

    delay(60000);
  }
}
