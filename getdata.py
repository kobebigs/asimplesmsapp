#!/usr/bin/env python

import os
import time
import serial

from pylab import * 
from matplotlib.mlab import csv2rec

from sendsms import TextMessage

def getAnalyseDataFromArduino():

	#connect to arduino serial
	uno = serial.Serial('/dev/ttyACM1', 9600) 

	#specify column titles
	with open('data.txt', 'w+') as output:
		output.write('time\ttemp\n')

	while True:
		recTime = time.strftime('%Y-%m-%d %H:%M:%S')

		valTemp = uno.readline()

		value = recTime + '\t' + valTemp
		print value #----temp

		if valTemp != "":
			with open('data.txt', 'a+') as f:
				f.write(value)	

			#initiate modem connection
			sambaConnect = TextMessage()
			sambaConnect.connectModem()

			#read contacts
			contact = csv2rec('contacts.txt', delimiter='\t', converterd={'phone':str})

			#typecast valTemp to an integer
			valTemp = int(valTemp)
			
			#check max/min temperature values and send SMS
			if valTemp < 10:	#low temperature
				#set recipents and process message
				for name, phone in contact:
					#create message using temperature values and name of contacts
					signature = "-- From Kobe's Lab"
					message = "Temperature Alert "+ name +",\nLow Temperature: " + str(valTemp)+ "\n" + recTime + "\n" + signature
				
					sambaConnect.setRecipient(phone)
					sambaConnect.setContent(message)
					sambaConnect.sendMessage()
					#print message
						
			elif valTemp > 30:				
				#set recipents and process message
				for name, phone in contact:
					#create message using temperature values and name of contacts
					signature = "-- From Kobe's Lab"
					message = "Temperature Alert "+ name +",\nHigh Temperature: " + str(valTemp)+ "\n" + recTime + "\n" + signature
					
					sambaConnect.setRecipient(phone)
					sambaConnect.setContent(message)
					sambaConnect.sendMessage()
					#print message
			else:	
				#Cool Temperature --- do nothing	
				print "cool temperature: ", recTime, valTemp

			#disconnect modem
			sambaConnect.disconnectModem();
		else:
			print "no recoreded temperature value"		

if __name__ == "__main__":
	getAnalyseDataFromArduino()
