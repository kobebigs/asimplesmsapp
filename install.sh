#!/bin/bash
# a script to install all the needed packages for app to run
# for setting up samba, web and arduino

#update and upgrade package sources
sudo apt-get update
sudo apt-get upgrade

#install python and other python frameworks & libraries
echo 'Installing needed python libraries ...'
sudo apt-get install -y --no-install-recommends python python-numpy python-matplotlib python-virtualenv python-serial python-pip build-essential

#create virtual environment.
virtualenv venv
. venv/bin/activate

#install flask
pip install Flask

#openjdk installation
sudo apt-get install -y openjdk-6-jre

sudo update-alternatives --config java

sudo apt-get install -y gcc-avr avr-libc

#install arduino
echo 'Downloading arduino...'
# Make tmp directory
if [ -e $HOME/tmp ]; then
    mkdir -p $HOME/tmp
else
    continue
fi

cd $HOME/tmp
# Download Debian file that matches system architecture
if [ $(uname -i) = 'i386' ]; then
    wget http://ftp.us.debian.org/debian/pool/main/a/arduino/arduino_1.0.4+dfsg-2_all.deb
    wget http://ftp.us.debian.org/debian/pool/main/a/arduino/arduino-core_1.0.4+dfsg-2_all.deb
    #install librxtx-java
	wget http://ftp.us.debian.org/debian/pool/main/r/rxtx/librxtx-java_2.2pre2-11_i386.deb
elif [ $(uname -i) = 'x86_64' ]; then
    wget http://ftp.us.debian.org/debian/pool/main/a/arduino/arduino_1.0.4+dfsg-2_all.deb
    wget http://ftp.us.debian.org/debian/pool/main/a/arduino/arduino-core_1.0.4+dfsg-2_all.deb
	#install librxtx-java
    wget http://ftp.us.debian.org/debian/pool/main/r/rxtx/librxtx-java_2.2pre2-11_ia64.deb
fi

# Install the package
echo 'Installing Arduino...'
sudo dpkg -i *.deb
sudo apt-get install -fy
# Cleanup and finish
rm *.deb
cd
echo 'Done.'
